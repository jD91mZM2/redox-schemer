#![no_std]

extern crate alloc;

use alloc::{
    string::String,
    sync::Arc,
};
use core::{
    sync::atomic::{AtomicUsize, Ordering::SeqCst},
};

use hashbrown::HashMap;
use lock_api::{RawMutex, Mutex};
use syscall::{Error, Result, SchemeBlock};

pub trait Resource {
    fn dup(&mut self, buf: &[u8], into: HandleId) -> Result<Option<Self>>
    where
        Self: Sized,
    {
        Err(Error::new(syscall::ENOSYS))
    }

    fn pread(&mut self, position: usize, buf: &[u8]) -> Result<Option<usize>> {
        Err(Error::new(syscall::ENOSYS))
    }
}
pub trait Scheme {
    type Resource: Resource;

    fn open(&mut self, path: &[u8], flags: usize, uid: u32, gid: u32, into: HandleId) -> Result<Option<Self::Resource>>;
    fn rmdir(&mut self, path: &[u8], uid: u32, gid: u32) -> Result<Option<()>> {
        Err(Error::new(syscall::ENOSYS))
    }
    fn unlink(&mut self, path: &[u8], uid: u32, gid: u32) -> Result<Option<()>> {
        Err(Error::new(syscall::ENOSYS))
    }
}

struct Handle<T: Resource> {
    inner: T,
    position: usize,
    flags: usize,
}
impl<T: Resource> Handle<T> {
    fn new(inner: T, flags: usize) -> Self {
        Self {
            inner,
            position: 0,
            flags,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct HandleId(usize);

pub struct Schemer<R: RawMutex, T: Scheme> {
    scheme: Mutex<R, T>,
    handles: Mutex<R, HashMap<HandleId, Arc<Mutex<R, Handle<T::Resource>>>>>,
    next_id: AtomicUsize,
}
impl<R: RawMutex, T: Scheme> Schemer<R, T> {
    pub fn for_scheme(scheme: T) -> Self {
        Self {
            scheme: Mutex::new(scheme),
            handles: Mutex::new(HashMap::new()),
            next_id: AtomicUsize::new(0),
        }
    }
}
impl<R: RawMutex, T: Scheme> SchemeBlock for Schemer<R, T> {
    fn open(&self, path: &[u8], flags: usize, uid: u32, gid: u32) -> Result<Option<usize>> {
        let id = HandleId(self.next_id.fetch_add(1, SeqCst));

        let resource = {
            let mut scheme = self.scheme.lock();
            match scheme.open(path, flags, uid, gid, id)? {
                Some(resource) => resource,
                None => return Ok(None),
            }
        };

        let mut handles = self.handles.lock();
        handles.insert(id, Arc::new(Mutex::new(Handle::new(resource, flags))));
        Ok(Some(id.0))
    }
    fn chmod(&self, path: &[u8], mode: u16, uid: u32, gid: u32) -> Result<Option<usize>> {
        let resource_id = match self.open(path, mode as usize, uid, gid)? {
            Some(id) => id,
            None => return Ok(None),
        };
        if self.fchmod(resource_id, mode)?.is_none() {
            return Ok(None);
        }
        self.close(resource_id)
    }
    fn rmdir(&self, path: &[u8], uid: u32, gid: u32) -> Result<Option<usize>> {
        let mut scheme = self.scheme.lock();
        Ok(scheme.rmdir(path, uid, gid)?.map(|()| 0))
    }
    fn unlink(&self, path: &[u8], uid: u32, gid: u32) -> Result<Option<usize>> {
        let mut scheme = self.scheme.lock();
        Ok(scheme.unlink(path, uid, gid)?.map(|()| 0))
    }
    fn dup(&self, old_id: usize, buf: &[u8]) -> Result<Option<usize>> {
        assert!(!buf.is_empty(), "if the buffer is empty, the kernel should handle that by itself");

        let old_handle = {
            let handles = self.handles.lock();
            let handle = handles.get(&HandleId(old_id)).ok_or_else(|| Error::new(syscall::ENOENT))?;
            Arc::clone(handle)
        };
        let new_id = HandleId(self.next_id.fetch_add(1, SeqCst));
        let (new_resource, old_flags) = {
            let old_handle = old_handle.lock();
            let old_flags = old_handle.flags;
            (match old_handle.inner.dup(buf, new_id)? {
                Some(new) => new,
                None => return Ok(None),
            }, old_flags)
        };

        let handles = self.handles.lock();
        handles.insert(new_id, Arc::new(Mutex::new(Handle::new(new_resource, old_flags))));
        Ok(Some(new_id.0))
    }
    fn read(&self, id: usize, buf: &mut [u8]) -> Result<Option<usize>> {
        let handle = {
            let handles = self.handles.lock();
            let handle = handles.get(&HandleId(id)).ok_or_else(|| Error::new(syscall::ENOENT))?;
            Arc::clone(handle)
        };
        let handle = handle.lock();
        let count = match handle.inner.pread(handle.position, buf)? {
            Some(count) => count,
            None => return Ok(None),
        };
        handle.position += count;
        Ok(Some(count))
    }
    fn write(&self, id: usize, buf: &[u8]) -> Result<Option<usize>> {
        let handle = {
            let handles = self.handles.lock();
            let handle = handles.get(&HandleId(id)).ok_or_else(|| Error::new(syscall::ENOENT))?;
            Arc::clone(handle)
        };
        let handle = handle.lock();
        if handle.flags & syscall::O_APPEND == syscall::O_APPEND {
            let stat = match handle.inner.stat(handle.position, buf)? {
                Some(stat) => stat,
                None => return Ok(None),
            };
            handle.position = handle.fstat(st_size);
        }
        let count = match handle.inner.pwrite(handle.position, buf)? {
            Some(count) => count,
            None => return Ok(None),
        };
        handle.position += count;
        Ok(Some(count))
    }
    fn seek(&self, id: usize, pos: usize, whence: usize) -> Result<Option<usize>>
    fn fchmod(&self, id: usize, mode: u16) -> Result<Option<usize>>
    fn fchown(&self, id: usize, uid: u32, gid: u32) -> Result<Option<usize>>
    fn fcntl(&self, id: usize, cmd: usize, arg: usize) -> Result<Option<usize>>
    fn fevent(&self, id: usize, flags: EventFlags) -> Result<Option<EventFlags>>
    fn fmap(&self, id: usize, map: &Map) -> Result<Option<usize>>
    fn funmap(&self, address: usize) -> Result<Option<usize>>
    fn fpath(&self, id: usize, buf: &mut [u8]) -> Result<Option<usize>>
    fn frename(
        &self,
        id: usize,
        path: &[u8],
        uid: u32,
        gid: u32
    ) -> Result<Option<usize>>
    fn fstat(&self, id: usize, stat: &mut syscall::Stat) -> Result<Option<usize>>
    fn fstatvfs(&self, id: usize, stat: &mut StatVfs) -> Result<Option<usize>>
    fn fsync(&self, id: usize) -> Result<Option<usize>>
    fn ftruncate(&self, id: usize, len: usize) -> Result<Option<usize>>
    fn futimens(&self, id: usize, times: &[TimeSpec]) -> Result<Option<usize>>
    fn close(&self, id: usize) -> Result<Option<usize>>}
